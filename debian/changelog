libpdl-graphics-gnuplot-perl (2.032-2) unstable; urgency=medium

  * Add libpdl-transform-color-perl to (build) dependencies.
  * Add self to Uploaders per sebastic suggestion.

 -- Ed J <etj@cpan.org>  Tue, 04 Feb 2025 17:37:35 +0000

libpdl-graphics-gnuplot-perl (2.032-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 22 Nov 2024 16:52:27 +0100

libpdl-graphics-gnuplot-perl (2.031-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 28 Oct 2024 15:16:55 +0100

libpdl-graphics-gnuplot-perl (2.030-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Require at least pdl 1:2.093.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 01 Oct 2024 15:19:32 +0200

libpdl-graphics-gnuplot-perl (2.029-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 25 Sep 2024 17:49:07 +0200

libpdl-graphics-gnuplot-perl (2.028-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Sep 2024 18:16:54 +0200

libpdl-graphics-gnuplot-perl (2.027-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.7.0, no changes.
  * Drop pod2man.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 31 Jul 2024 21:16:12 +0200

libpdl-graphics-gnuplot-perl (2.026-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Use dh-sequence-pdl for dh_pdl.
  * Enable Salsa CI.
  * Add patch to fix pod2man errors.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 22 Apr 2024 11:31:56 +0200

libpdl-graphics-gnuplot-perl (2.024-1) unstable; urgency=medium

  * Team upload.
  * Bump debhelper compat to 13.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 11 Jun 2023 17:27:19 +0200

libpdl-graphics-gnuplot-perl (2.024-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Ignore test failures instead of not running tests.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 01 Apr 2023 18:56:40 +0200

libpdl-graphics-gnuplot-perl (2.023-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Feb 2023 16:16:26 +0100

libpdl-graphics-gnuplot-perl (2.022-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes.
  * Replace gnuplot (build) dependencies with non-metapackages.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 28 Jan 2023 18:18:47 +0100

libpdl-graphics-gnuplot-perl (2.021-2) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.1, no changes.
  * Add Rules-Requires-Root to control file.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 02 Dec 2022 09:06:29 +0100

libpdl-graphics-gnuplot-perl (2.021-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 02 Mar 2022 15:59:18 +0100

libpdl-graphics-gnuplot-perl (2.020-1) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * New upstream release.
  * Bump Standards-Version to 4.6.0, no changes.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libalien-gnuplot-perl.
    + libpdl-graphics-gnuplot-perl: Drop versioned constraint on
      libalien-gnuplot-perl in Depends.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 14 Feb 2022 07:09:25 +0100

libpdl-graphics-gnuplot-perl (2.019-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update upstream metadata.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 21 Aug 2021 16:40:05 +0200

libpdl-graphics-gnuplot-perl (2.018-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 18:47:39 +0200

libpdl-graphics-gnuplot-perl (2.018-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 12 Aug 2021 15:37:08 +0200

libpdl-graphics-gnuplot-perl (2.017-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 01 Jun 2021 06:36:53 +0200

libpdl-graphics-gnuplot-perl (2.016-1~exp1) experimental; urgency=medium

  [ Bas Couwenberg ]
  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on pdl.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 10 Apr 2021 17:17:50 +0200

libpdl-graphics-gnuplot-perl (2.013-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.5.0, no changes.
  * Bump debhelper compat to 10.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Nov 2020 06:58:38 +0100

libpdl-graphics-gnuplot-perl (2.013-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 14 Dec 2019 08:22:26 +0100

libpdl-graphics-gnuplot-perl (2.012-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.4.1, no changes.
  * Update gbp.conf to use --source-only-changes by default.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 06 Dec 2019 07:21:29 +0100

libpdl-graphics-gnuplot-perl (2.011-3) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.1.5, no changes.
  * Update Vcs-* URLs for Salsa.
  * Strip trailing whitespace from rules file.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 22 Jul 2018 19:26:06 +0200

libpdl-graphics-gnuplot-perl (2.011-2) unstable; urgency=medium

  * Team upload.
  * Require at least libalien-gnuplot-perl 1.031.
    (closes: #870090)
  * Bump Standards-Version to 4.0.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 01 Aug 2017 16:48:25 +0200

libpdl-graphics-gnuplot-perl (2.011-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 18 Jun 2017 15:58:57 +0200

libpdl-graphics-gnuplot-perl (2.011-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop spelling-errors.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 27 May 2017 12:56:56 +0200

libpdl-graphics-gnuplot-perl (2.005-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Bas Couwenberg ]
  * Restructure control file with cme.
  * Enable all hardening buildflags.
  * Add upstream metadata.
  * Add gbp.conf to use pristine-tar by default.
  * Use dh_pdl to set versioned pdl dependency via ${pdl:Depends}.
  * Bump Standards-Version to 3.9.8, no changes.
  * Add patch to fix spelling errors.
  * Require at least pdl 2.016 for pdl transition.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 24 Jun 2016 13:00:25 +0200

libpdl-graphics-gnuplot-perl (2.005-2) unstable; urgency=medium

  * Team upload.
  * Make the package autopkgtestable, but skip the main test
    suite for now.

 -- Niko Tyni <ntyni@debian.org>  Fri, 25 Sep 2015 11:50:50 +0300

libpdl-graphics-gnuplot-perl (2.005-1) unstable; urgency=medium

  * Imported Upstream version 2.005

    - better detection of newer gnuplot builds

 -- Dima Kogan <dima@secretsauce.net>  Tue, 25 Aug 2015 11:26:07 -0700

libpdl-graphics-gnuplot-perl (2.003-1) unstable; urgency=low

  * Initial Release (Closes: #762064)

 -- Dima Kogan <dima@secretsauce.net>  Wed, 17 Sep 2014 21:38:05 -0700
